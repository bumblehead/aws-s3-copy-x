const s3copyPartition = require('./s3-copy-partition');
const s3copyutil = require('./s3-copy-util');
const s3copyopts = require('./s3-copy-opts');
const s3copyaws = require('./s3-copy-aws');

const uploadParts = (() => {
  const end = (opts, fn, err, success) => {
    if (err || fn.name === 'nextPart' || opts.activeparts === 0)
      fn(err, success);
  };

  return function nextPart (s3, opts, fn) {
    let { pos } = opts;

    if (!s3copyPartition.isPartitionRange(opts, pos))
      return end(opts, fn, null, 'FINISH');

    if (opts.activeparts > opts.maxparts)
      return null;

    opts.activeparts += 1;

    s3copyaws.uploadPartCopy(s3, opts, opts.pos++, (err, res) => {
      if (err || res === 'FINISH')
        return end(opts, fn, err, res);

      opts.activeparts -= 1;
      opts.completedparts += 1;
      opts.completedpartsarr[pos] = res.CopyPartResult;
      opts.info([ 'part', ...s3copyutil.getMetrics(opts) ]);

      nextPart(s3, opts, fn);
    });

    nextPart(s3, opts, fn);
  };
})();

const errorAbort = (s3, opts, error, fn) => {
  opts.error({ error });

  s3copyaws.abortMultipartUpload(s3, opts, (abortErr, abortRes) => {
    opts.error(abortErr ? { abortErr } : { abortRes });

    fn(error);
  });
};

const copyObject = (s3, opts, fn) => {
  s3copyaws.headObject(s3, opts, (err, res) => {
    if (err) return fn(err);

    opts.fullsize = res.ContentLength;
    opts = s3copyopts(opts);

    if (opts.fullsize < 10000000)
      return s3copyaws.copyObject(s3, opts, fn);

    s3copyaws.createMultipartUpload(s3, opts, (err, res) => {
      if (err) return fn(err);

      opts.UploadId = res.UploadId;

      uploadParts(s3, opts, err => {
        if (err) return errorAbort(s3, opts, err, fn);

        s3copyaws.completeMultipartUpload(s3, opts, (err, res) => {
          if (err) return errorAbort(s3, opts, err, fn);

          fn(err, res);
        });
      });
    });
  });
};

const copyObjects = (s3, opts, objects, fn, index = 0) => {
  if (index >= objects.length)
    return fn(null, objects);

  opts.info([ 'object', index + 1, objects.length, objects[index].Key ]);
  copyObject(s3, Object.assign({}, opts, {
    CopySource : [
      s3copyaws.getSourceBucket(opts.CopySource),
      objects[index].Key ].join('/'),
    Key : objects[index].Key,
    fullsize : objects[index].Size
  }), err => {
    if (err) return fn(err);

    copyObjects(s3, opts, objects, fn, ++index);
  });
};

const removeObject = (s3, opts, fn) => {
  opts.info([ 'rm', 1, 1, s3copyaws.getSourcePath(opts.CopySource) ]);
  s3copyaws.deleteObject(s3, {
    Bucket : s3copyaws.getSourceBucket(opts.CopySource),
    Key : s3copyaws.getSourcePath(opts.CopySource)
  }, fn);
};

const removeObjects = (s3, opts, objects, fn, index = 0) => {
  if (index >= objects.length)
    return fn(null, objects);

  opts.info([ 'rm', index + 1, objects.length, objects[index].Key ]);
  s3copyaws.deleteObject(s3, {
    Bucket : s3copyaws.getSourceBucket(opts.CopySource),
    Key : objects[index].Key
  }, err => {
    if (err) return fn(err);

    removeObjects(s3, opts, objects, fn, ++index);
  });
};

const copyDirectory = (s3, opts, fn) => {
  s3copyaws.listObjectsDeep(s3, opts, (err, objects) => {
    if (err) return fn(err);

    copyObjects(s3, opts, objects, (err, objects) => {
      if (err) return fn(err);

      return opts.CopySourceRemove
        ? removeObjects(s3, opts, objects, fn)
        : fn(null, objects);
    });
  });
};

const copyKey = (s3, opts, fn) => {
  opts.info([ 'object', 1, 1, opts.Key ]);
  copyObject(s3, opts, (err, object) => {
    if (err) return fn(err);

    return opts.CopySourceRemove
      ? removeObject(s3, opts, fn)
      : fn(null, object);
  });
};

const copy = (s3, opts, fn) => {
  if (!s3copyaws.isS3Valid(s3))
    throw new Error('invalid s3');

  return (/\/$/.test(opts.CopySource) && !opts.Key)
    ? copyDirectory(s3, s3copyopts(opts), fn)
    : copyKey(s3, s3copyopts(opts), fn);
};

const copyPromise = (s3, opts) => new Promise((resolve, reject) => {
  copy(s3, opts, (err, res) => err
    ? reject(err)
    : resolve(res));
});

module.exports = copyPromise;
