// is the pos within range of object's full size?
//
const isPartitionRange = (opts, pos = 0) =>
  pos * opts.partsize < opts.fullsize;

// [
//   "0-49999999",
//   "50000000-99999999",
//   // ...
//   "7700000000-7746468799"
// ]
const getPartition = (opts, pos = 0, bgn = pos * opts.partsize) =>
  [ bgn, Math.min(bgn + opts.partsize, opts.fullsize) - 1 ];

// "bytes=7700000000-7746468799"
//
const getPartitionStr = (opts, pos = 0) =>
  `bytes=${getPartition(opts, pos).join('-')}`;

module.exports = {
  isPartitionRange,
  getPartition,
  getPartitionStr
};
