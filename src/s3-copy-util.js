const rnd = num => Math.round(num * 100) / 100;

const toMB = byteNum => rnd(byteNum / 1000000);

const getMetrics = opts => ([
  toMB(Math.min(opts.partsize * opts.completedparts, opts.fullsize)),
  toMB(opts.fullsize),
  rnd(Math.min(100 * opts.partsize * opts.completedparts / opts.fullsize, 100))
]);

module.exports = {
  rnd,
  toMB,
  getMetrics
};
