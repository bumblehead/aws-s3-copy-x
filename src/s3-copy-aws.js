const s3copyPartition = require('./s3-copy-partition');

const isS3Valid = s3 => Boolean(
  s3 &&
  s3.api &&
  s3.api.fullName === 'Amazon Simple Storage Service');

const filterParam = params => Object.keys(params).reduce((filtered, key) => (
  typeof params[key] !== 'undefined'
    ? Object.assign({ [key] : params[key] }, filtered)
    : filtered
), {});

// ex, getSourceBucket('bucket/path/to/source.ext') => 'bucket'
//
const getSourceBucket = source => source
  .substring(0, source.indexOf('/'));

// ex, getSourcePath('bucket/path/to/source.ext') => 'path/to/source.ext'
//
const getSourcePath = source => source
  .substring(source.indexOf('/') + 1);

const headObject = (s3, opts, fn) => s3.headObject(
  filterParam({
    Bucket : getSourceBucket(opts.CopySource),
    Key : getSourcePath(opts.CopySource)
  }), fn);

const copyObject = (s3, opts, fn) => s3.copyObject(
  filterParam({
    Bucket : opts.Bucket,
    Key : opts.Key,
    ACL : opts.ACL,
    CopySource : encodeURIComponent(opts.CopySource),
    ContentType : opts.ContentType
  }), fn);

const listObjects = (s3, opts, fn) => s3.listObjectsV2(
  filterParam({
    Bucket : opts.Bucket,
    MaxKeys : opts.MaxKeys, // default is 1,000
    Delimiter : '/',
    Prefix : opts.Prefix || getSourcePath(opts.CopySource),
    ContinuationToken : opts.ContinuationToken
  }), fn);

const listObjectsAllKeys = (s3, opts, fn, truncatedContents = []) =>
  listObjects(s3, opts, (err, res) => {
    res.Contents = res.Contents.concat(truncatedContents);

    if (res.IsTruncated && res.NextContinuationToken) {
      listObjectsAllKeys(s3, Object.assign({
        ContinuationToken : res.NextContinuationToken
      }, opts), fn, res.Contents);
    } else {
      fn(err, res);
    }
  });

const listObjectsDeep = (s3, opts, fn, prefixes = [ {
  Prefix : getSourcePath(opts.CopySource) // start at CopySource directory
} ], objects = []) => {
  if (prefixes.length === 0)
    return fn(null, objects);

  listObjectsAllKeys(s3, Object.assign({}, opts, {
    Bucket : getSourceBucket(opts.CopySource),
    Prefix : prefixes[0].Prefix
  }), (err, res) => {
    if (err) return fn(err);

    listObjectsDeep(s3, opts, (err, objects) => {
      if (err) return fn(err);

      listObjectsDeep(s3, opts, fn, prefixes.slice(1), objects);
    }, res.CommonPrefixes, objects.concat(res.Contents));
  });
};

const createMultipartUpload = (s3, opts, fn) => s3.createMultipartUpload(
  filterParam({
    Bucket : opts.Bucket,
    Key : opts.Key,
    ACL : opts.ACL,
    Expires : opts.Expires,
    ContentType : opts.ContentType,
    ServerSideEncryption : opts.ServerSideEncryption
  }), fn);

const uploadPartCopy = (s3, opts, pos, fn) => s3.uploadPartCopy(
  filterParam({
    UploadId : opts.UploadId,
    Bucket : opts.Bucket,
    Key : opts.Key,
    CopySource : encodeURIComponent(opts.CopySource),
    CopySourceRange : s3copyPartition.getPartitionStr(opts, pos),
    PartNumber : pos + 1
  }), fn);

const abortMultipartUpload = (s3, opts, fn) => s3.abortMultipartUpload(
  filterParam({
    UploadId : opts.UploadId,
    Bucket : opts.Bucket,
    Key : opts.Key
  }), fn);

const completeMultipartUpload = (s3, opts, fn) => s3.completeMultipartUpload(
  filterParam({
    UploadId : opts.UploadId,
    Bucket : opts.Bucket,
    Key : opts.Key,
    MultipartUpload : {
      Parts : opts.completedpartsarr.map(({ ETag }, i) => ({
        ETag,
        PartNumber : i + 1
      }))
    }
  }), fn);

const deleteObject = (s3, opts, fn) => s3.deleteObject(
  filterParam({
    Bucket : opts.Bucket,
    Key : opts.Key
  }), fn);

module.exports = {
  isS3Valid,
  filterParam,
  getSourceBucket,
  getSourcePath,
  headObject,
  copyObject,
  listObjects,
  listObjectsDeep,
  createMultipartUpload,
  uploadPartCopy,
  abortMultipartUpload,
  completeMultipartUpload,
  deleteObject
};
