const mime = require('mime-types');
const castas = require('castas');
const s3copyaws = require('./s3-copy-aws');

module.exports = opt => {
  let finopt = {};

  opt = opt || {};

  finopt.partsize = castas.num(opt.partsize, 50000000); // 50mb
  finopt.fullsize = castas.num(opt.fullsize, 0);
  finopt.maxparts = castas.num(opt.maxparts, 4);

  finopt.completedpartsarr = [];
  finopt.completedparts = 0;
  finopt.activeparts = 0;
  finopt.pos = 0;

  finopt.error = typeof opt.error === 'function' ? opt.error : () => {};
  finopt.info = typeof opt.info === 'function' ? opt.info : () => {};

  finopt.ACL = castas.str(opt.ACL, 'private');
  finopt.Bucket = castas.str(opt.Bucket);
  finopt.Expires = castas.str(opt.Expires);
  finopt.CopySource = castas.str(opt.CopySource);
  finopt.Key = castas.str(
    opt.Key, s3copyaws.getSourcePath(finopt.CopySource || '') || undefined);
  finopt.CopySourceRemove = castas.bool(opt.CopySourceRemove, false);
  finopt.ContentType = castas.str(
    opt.ContentType, mime.lookup(opt.CopySource || opt.Key) ||
    'application/octet-stream');
  finopt.ServerSideEncryption = castas.str(opt.ServerSideEncryption);

  return finopt;
};
