const copypartition = require('../src/s3-copy-partition');

describe('s3-copy-partition.isPartitionRange', () => {
  it('should return true for partition in range', () => {
    expect(copypartition.isPartitionRange({
      partsize : 200,
      fullsize : 400
    }, 1)).toBe(true);
  });

  it('should return false for partition out of range', () => {
    expect(copypartition.isPartitionRange({
      partsize : 200,
      fullsize : 400
    }, 5)).toBe(false);
  });
});


describe('s3-copy-partition.getPartition', () => {
  it('should return [] for 0th 200 size partition', () => {
    expect(copypartition.getPartition({
      partsize : 200,
      fullsize : 450
    }, 1)).toEqual([ 200, 399 ]);
  });

  it('should return false for partition out of range', () => {
    expect(copypartition.getPartition({
      partsize : 200,
      fullsize : 450
    }, 2)).toEqual([ 400, 449 ]);
  });
});


describe('s3-copy-partition.getPartitionStr', () => {
  it('should return [] for 0th 200 size partition', () => {
    expect(copypartition.getPartitionStr({
      partsize : 200,
      fullsize : 450
    }, 1)).toEqual('bytes=200-399');
  });

  it('should return false for partition out of range', () => {
    expect(copypartition.getPartitionStr({
      partsize : 200,
      fullsize : 450
    }, 2)).toEqual('bytes=400-449');
  });
});
