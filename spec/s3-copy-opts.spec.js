const copyopts = require('../src/s3-copy-opts');

describe('s3-copy-opts', () => {
  it('should return sane defaults', () => {
    let defaultopts = copyopts();

    expect(defaultopts.partsize).toEqual(50000000);
    expect(defaultopts.fullsize).toEqual(0);
    expect(defaultopts.maxparts).toEqual(4);

    expect(defaultopts.Key).toEqual(undefined);
    expect(defaultopts.Bucket).toEqual(undefined);
    expect(defaultopts.Expires).toEqual(undefined);
    expect(defaultopts.CopySource).toEqual(undefined);
    expect(defaultopts.ContentType).toEqual('application/octet-stream');
    expect(defaultopts.ServerSideEncryption).toEqual(undefined);
  });
});
