const copy = require('../src/s3-copy');
const stubs = require('./stubs');

describe('s3-copy', () => {
  it('should copy from one bucket to another', done => {
    let s3 = { api : { fullName : 'Amazon Simple Storage Service' } };

    s3.headObject = (opts, fn) =>
      fn(null, stubs.headObject_success);

    s3.createMultipartUpload = (opts, fn) =>
      fn(null, stubs.createMultipartUpload_success);

    s3.uploadPartCopy = (opts, fn) =>
      fn(null, stubs.uploadPartCopy_success);

    s3.completeMultipartUpload = (opts, fn) =>
      fn(null, stubs.completeMultipartUpload_success);

    copy(s3, {
      Key : 'Key.mp4',
      Bucket : 'Bucket',
      CopySource : 'CopySource',
      info : () => {}
    }).then(res => {
      expect(res.Key).toBe('key.mp4');

      done();
    });
  });

  it('should use s3.copyObject if object length less than 10MB', done => {
    let s3 = { api : { fullName : 'Amazon Simple Storage Service' } };

    s3.headObject = (opts, fn) =>
      fn(null, Object.assign({}, stubs.headObject_success, {
        ContentLength : 9999999
      }));

    s3.copyObject = (opts, fn) =>
      fn(null, stubs.copyObject_success);

    copy(s3, {
      Key : 'Key.mp4',
      Bucket : 'Bucket',
      CopySource : 'CopySource',
      info : () => {}
    }).then(res => {
      expect(typeof res.CopyObjectResult).toBe('object');

      done();
    });
  });

  it('should abort multipart upload if there is an error', done => {
    let s3 = { api : { fullName : 'Amazon Simple Storage Service' } };

    s3.headObject = (opts, fn) =>
      fn(null, stubs.headObject_success);

    s3.createMultipartUpload = (opts, fn) =>
      fn(null, stubs.createMultipartUpload_success);

    s3.uploadPartCopy = (opts, fn) =>
      fn(stubs.uploadPartCopy_internalError);

    s3.abortMultipartUpload = (opts, fn) =>
      fn(null, stubs.abortMultipartUpload_success);

    copy(s3, {
      Key : 'Key.mp4',
      Bucket : 'Bucket',
      CopySource : 'CopySource',
      info : () => {},
      error : () => {}
    }).catch(err => {
      expect(err.code).toBe('InternalError');

      done();
    });
  });

});
