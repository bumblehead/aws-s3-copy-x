const copyutil = require('../src/s3-copy-util');
const BYTES_MB = 1000000;

describe('s3-copy-util.toRound', () => {
  it('should return rounded number', () => {
    expect(copyutil.rnd(30.001)).toEqual(30);
  });
});

describe('s3-copy-util.toMB', () => {
  it('should return 30MB from 30000001', () => {
    expect(copyutil.toMB(30 * BYTES_MB * 1.0000000000001)).toEqual(30);
  });
});

describe('s3-copy-util.getMetrics', () => {
  it('should return valid metrics', () => {
    expect(copyutil.getMetrics({
      partsize : 100 * BYTES_MB,
      fullsize : 600 * BYTES_MB,
      completedparts : 3
    })).toEqual([ 300, 600, 50 ]);
  });
});
