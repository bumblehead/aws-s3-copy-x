const headObject_success = {
  AcceptRanges : 'bytes',
  LastModified : new Date(),
  ContentLength : 7746468800,
  ETag : '"91398c5f162d849e03ceb85515c998cf-739"',
  ContentType : 'video/mp4',
  Metadata : {}
};

const headObject_notfound = {
  message : null,
  code : 'NotFound',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 404,
  retryable : false,
  retryDelay : 87.30587163739487
};

const copyObject_success = {
  CopyObjectResult : {
    ETag : '"6805f2cfc46c0f04559748bb039d69ae"',
    LastModified : new Date()
  }
};

const copyObject_invalidRequest = {
  message : [
    'The specified copy source is larger than the maximum allowable',
    'size for a copy source: 5368709120'
  ].join(' '),
  code : 'InvalidRequest',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 400,
  retryable : false,
  retryDelay : 2.10034588670609
};

const createMultipartUpload_success = {
  Bucket : 'Bucket',
  Key : 'Key.mp4',
  UploadId : 'UploadId'
};

const createMultipartUpload_notfound = {
  message : 'The specified bucket does not exist',
  code : 'NoSuchBucket',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 404,
  retryable : false,
  retryDelay : 190.128680980159
};

const uploadPartCopy_success = {
  CopyPartResult : {
    ETag : '"938edd172908b5328f71f367c3a65f83"',
    LastModified : new Date()
  }
};

const uploadPartCopy_internalError = {
  message : 'We encountered an internal error. Please try again.',
  code : 'InternalError',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 500,
  retryable : true
};

const uploadPartCopy_slowDown = {
  message : 'Please reduce your request rate.',
  code : 'SlowDown',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 503,
  retryable : true
};

// aws-sdk object returns enty object on success
const abortMultipartUpload_success = {};

const abortMultipartUpload_noSuchUpload = {
  message : [
    'The specified upload does not exist. The upload ID may be invalid,',
    'or the upload may have been aborted or completed.' ].join(' '),
  code : 'NoSuchUpload',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 404,
  retryable : false,
  retryDelay : 7.773530623506764
};

const completeMultipartUpload_success = {
  Location : 'https://bucket/key.mp4',
  Bucket : 'bucket',
  Key : 'key.mp4',
  ETag : '"c9845690562a76c6c669d5ebc6479cd8-6"'
};

const completeMultipartUpload_noSuchUpload = {
  message : [
    'The specified upload does not exist. The upload ID may be invalid,',
    'or the upload may have been aborted or completed.' ].join(' '),
  code : 'NoSuchUpload',
  region : null,
  time : new Date(),
  requestId : 'requestId',
  extendedRequestId : 'extendedRequestId',
  cfId : undefined,
  statusCode : 404,
  retryable : false,
  retryDelay : 0.36401321212209403
};

const deleteObject_success = {
  DeleteMarker : true,
  VersionId : 'W15JAOA7IGt467kFYH2Qu.aGCSGXokBu'
};

module.exports = {
  headObject_success,
  headObject_notfound,
  copyObject_success,
  copyObject_invalidRequest,
  createMultipartUpload_success,
  createMultipartUpload_notfound,
  uploadPartCopy_success,
  uploadPartCopy_internalError,
  uploadPartCopy_slowDown,
  abortMultipartUpload_success,
  abortMultipartUpload_noSuchUpload,
  completeMultipartUpload_success,
  completeMultipartUpload_noSuchUpload,
  deleteObject_success
};
