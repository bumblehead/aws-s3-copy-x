const copyaws = require('../src/s3-copy-aws');
const stubs = require('./stubs');

let s3 = { api : { fullName : 'Amazon Simple Storage Service' } };

describe('s3-copy-aws.isS3Valid', () => {
  it('should return true for probably valid s3 object', () => {
    expect(copyaws.isS3Valid({
      api : { fullName : 'Amazon Simple Storage Service' }
    })).toBe(true);
  });

  it('should return false for probably valid s3 object', () => {
    expect(copyaws.isS3Valid()).toBe(false);
  });
});

describe('s3-copy-aws.filterParam', () => {
  it('should remove undefined properties from the object', () => {
    expect(copyaws.filterParam({
      prop1 : undefined,
      prop2 : false,
      prop3 : '',
      prop4 : 0
    })).toEqual({
      prop2 : false,
      prop3 : '',
      prop4 : 0
    });
  });
});

describe('s3-copy-aws.getSourceBucket', () => {
  it('should return "bucket" from "bucket/key"', () => {
    expect(copyaws.getSourceBucket('bucket/key')).toBe('bucket');
  });
});

describe('s3-copy-aws.getSourcePath', () => {
  it('should return "key" from "bucket/key"', () => {
    expect(copyaws.getSourcePath('bucket/key')).toBe('key');
  });
});

describe('s3-copy-aws.headObject', () => {
  s3.headObject = (opts, fn) => (
    opts.Bucket === 'Bucket' && opts.Key === 'Key'
      ? fn(null, stubs.headObject_success)
      : fn(stubs.headObject_notfound));

  it('should return success as second param', done => {
    copyaws.headObject(s3, {
      CopySource : 'Bucket/Key'
    }, (err, res) => {
      expect(typeof res.ContentLength).toBe('number');
      done();
    });
  });

  it('should return error as first param', done => {
    copyaws.headObject(s3, {
      CopySource : 'Bucket/InvalidKey'
    }, err => {
      expect(err.code).toBe('NotFound');
      done();
    });
  });
});

describe('s3-copy-aws.copyObject', () => {
  s3.copyObject = (opts, fn) => (
    opts.CopySource === 'CopySource'
      ? fn(null, stubs.copyObject_success)
      : fn(stubs.copyObject_invalidRequest));

  it('should return success as second param', done => {
    copyaws.copyObject(s3, {
      CopySource : 'CopySource',
      Bucket : 'Bucket',
      Key : 'Key'
    }, (err, res) => {
      expect(typeof res.CopyObjectResult.ETag).toBe('string');
      done();
    });
  });

  it('should return error as first param', done => {
    copyaws.copyObject(s3, {
      CopySource : 'CopySource_TooBigFile',
      Bucket : 'Bucket',
      Key : 'Key'
    }, err => {
      expect(err.code).toBe('InvalidRequest');
      done();
    });
  });
});


describe('s3-copy-aws.createMultipartUpload', () => {
  s3.createMultipartUpload = (opts, fn) => (
    opts.Bucket === 'Bucket'
      ? fn(null, stubs.createMultipartUpload_success)
      : fn(stubs.createMultipartUpload_notfound));

  it('should return success as second param', done => {
    copyaws.createMultipartUpload(s3, {
      Bucket : 'Bucket'
    }, (err, res) => {
      expect(typeof res.UploadId).toBe('string');
      done();
    });
  });

  it('should return error as first param', done => {
    copyaws.createMultipartUpload(s3, {
      Bucket : 'InvalidBucket'
    }, err => {
      expect(err.code).toBe('NoSuchBucket');
      done();
    });
  });
});

describe('s3-copy-aws.uploadPartCopy', () => {
  s3.uploadPartCopy = (opts, fn) => (
    opts.PartNumber === 1 &&
    opts.Bucket === 'Bucket'
      ? fn(null, stubs.uploadPartCopy_success)
      : fn(stubs.uploadPartCopy_slowDown));

  it('should return success as second param', done => {
    copyaws.uploadPartCopy(s3, {
      UploadId : 'UploadId',
      CopySource : 'CopySource',
      Bucket : 'Bucket',
      Key : 'Key'
    }, 0, (err, res) => {
      expect(typeof res.CopyPartResult.ETag).toBe('string');
      done();
    });
  });
});


describe('s3-copy-aws.completeMultipartUpload', () => {
  s3.completeMultipartUpload = (opts, fn) => (
    opts.UploadId === 'UploadId'
      ? fn(null, stubs.completeMultipartUpload_success)
      : fn(stubs.completeMultipartUpload_noSuchUpload));

  it('should return success as second param', done => {
    copyaws.completeMultipartUpload(s3, {
      completedpartsarr : [],
      UploadId : 'UploadId',
      Bucket : 'Bucket',
      Key : 'Key'
    }, (err, res) => {
      expect(typeof res.Location).toBe('string');
      done();
    });
  });

  it('should return error as first param', done => {
    copyaws.completeMultipartUpload(s3, {
      completedpartsarr : [],
      UploadId : 'UploadIdInvalid',
      Bucket : 'Bucket',
      Key : 'Key'
    }, err => {
      expect(err.code).toBe('NoSuchUpload');
      done();
    });
  });
});


describe('s3-copy-aws.abortMultipartUpload', () => {
  s3.abortMultipartUpload = (opts, fn) => (
    opts.UploadId === 'UploadId'
      ? fn(null, stubs.abortMultipartUpload_success)
      : fn(stubs.abortMultipartUpload_notfound));

  it('should return success as second param', done => {
    copyaws.abortMultipartUpload(s3, {
      UploadId : 'UploadId',
      Bucket : 'Bucket',
      Key : 'Key'
    }, (err, res) => {
      expect(res && typeof res).toBe('object');
      done();
    });
  });
});
