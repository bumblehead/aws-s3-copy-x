aws-s3-copy-x
===============
**(c)[Bumblehead][0]**

[![aws-s3-copy-x](https://img.shields.io/npm/v/aws-s3-copy-x.svg)](https://www.npmjs.com/package/aws-s3-copy-x)
[![pipeline status](https://gitlab.com/bumblehead/aws-s3-copy-x/badges/master/pipeline.svg)](https://gitlab.com/bumblehead/aws-s3-copy-x/pipelines)
[![coverage report](https://gitlab.com/bumblehead/aws-s3-copy-x/badges/master/coverage.svg)](https://gitlab.com/bumblehead/aws-s3-copy-x/commits/master)


Use aws-sdk to multipart copy from one bucket to another.

If the CopySource definition ends with '/', for example 'SourceBucket/path/to/files/' and no Key is defined, CopySource is handled like a directory and all objects listed inside it are copied.

```javascript
const AWS = require('aws-sdk');
const s3Copy = require('aws-s3-copy-x');
const s3 = new AWS.S3({ maxRetries: 10 });

await s3Copy(s3, {
  ACL : 'bucket-owner-full-control',
  Key : 'Key.mp4',
  Bucket : 'Bucket',
  CopySource : 'SourceBucket/Key.mp4',
  info : msg => console.info(msg),
  error : msg => console.error(msg),
  partsize : 50000000 // default is 50000000 bytes (50MB)
  maxparts : 5, // default max number concurrent requests is 5
});
```


[0]: http://www.bumblehead.com                            "bumblehead"



![scrounge](https://github.com/iambumblehead/scroungejs/raw/master/img/hand.png)

(The MIT License)

Copyright (c) [Bumblehead][0] <chris@bumblehead.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   
